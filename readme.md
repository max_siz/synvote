
# Synvote - vote with winner reward 
![Smart contracts deployed on Mainnet network](https://img.shields.io/badge/Mainnet-deployed-green.svg)  

[First Mainnet deploy](https://etherscan.io/address/0x2c3177cfa17a8e10dfc2a39a572652601683d95c)  
## Usage
1. Just deploy it.
2. Add vote item from owner(deployer) address. Please call 
`addProjectToVote("prjName", "prjAddress")` for each item .
3. Call startVote() with vote finish date param (UnixDate).




## Usefull links
https://ethereum.stackexchange.com/questions/50310/how-to-pass-the-value-in-bytes32-array
