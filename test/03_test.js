const { latestTime } = require('./helpers/latestTime');
const { increaseTimeTo, duration } = require('./helpers/increaseTime');

var Synvotetoken = artifacts.require("./Synvote.sol");
var MyERC20;
var gasUsedwei=0;
var gasUsed=0;
var _currentStage;
var prname1='project_one'
var prname2='project_two'
var prname3='project_three'
var prname_wrong='project_four'
var _owner;
var _hash;
var _voteCount_before;
var _prjWeiRaised_before;
var _voteCount_after;
var _prjWeiRaised_after;
var votefinish;//=1546214400  //20181231
var vote_finish;
var event;
var balance_contract_before;
var balance_contract_after;
var balance_acc_before;
var balance_acc_after;
var _currentWinner_before;
var _currentWinner_after;
var vote_sum1=100000000000000000;
var balance_pr1_before;
var balance_pr1_after;
var pr1_account;
var pr2_account;
var pr3_account;
var date_block;




contract('Synvotetoken functionality tests - 03',async (accounts) => {

                 
    before( function() {
        return Synvotetoken.deployed().then(function(instance){
            MyERC20 = instance;
            return MyERC20.currentStage.call();
         }).then(function(callResult){
                   console.log("currentStage=", callResult);
                   _currentStage=callResult;
                       return MyERC20.owner.call();
          }).then(function(callResult){
                   console.log("owner=", callResult);
                    owner=callResult;
                    pr1_account=accounts[5];
                    pr2_account=accounts[6];
                    pr3_account=accounts[7];
                   return MyERC20.addProjectToVote(prname1, pr1_account,
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")});
         }).then(function(txResult){
               return MyERC20.addProjectToVote(prname2, pr2_account,
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")});
         }).then(function(txResult){
               return MyERC20.addProjectToVote(prname3, pr3_account,
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")});
         }).then(function(txResult){

                     return true;
        });
    });


     describe('define the date of the finish', function(){
            
        it('03.000_create the date of the finish ', async function () { 
                date_block = await latestTime();
                //console.log("date_block=", date_block);
                votefinish=date_block+duration.seconds(20);//20 seconds until the end of the voting
                //console.log("votefinish=", votefinish);
                return MyERC20.startVote(votefinish,
                                                    {from: owner, gasPrice: web3.toWei(1,"Gwei")}).then(function(txResult){
                return true;
           });
        });
    });

    describe('The projects were added to the list of the voting. The voting was started. Check the results', function(){
         
         it('03.00_to check stage', function() {
                return MyERC20.currentStage.call().then(function(callResult){
                //console.log("_currentStage=", callResult);
                assert.equal(callResult,1, 'ERROR:The stage is not inProgress' );
               //return true;
            });
        });


        it('03.01_to check the adding of the first project ', function() {
               return MyERC20.calculateSha3(prname1).then(function(callResult){
                //console.log("hash=", callResult);
                return MyERC20.projects(callResult);
            }).then(function(callResult){
                //console.log("prjAddress=", callResult);
                assert.equal(callResult[0],
                    pr1_account, 
                    'ERROR: The adding of project in the list of voting works with the mistake' );
            });
        });


        it('03.02_to check the adding of the second project', function() {
               return MyERC20.calculateSha3(prname2).then(function(callResult){
               // console.log("hash=", callResult);
                return MyERC20.projects(callResult);
            }).then(function(callResult){
              //  console.log("prjAddress=", callResult);
                assert.equal(callResult[0],
                    pr2_account, 
                    'ERROR: The adding of project in the list of voting work with the mistake' );
            });
        });

        it('03.03_to check the adding of the third project', function() {
               return MyERC20.calculateSha3(prname3).then(function(callResult){
                //console.log("hash=", callResult);
                return MyERC20.projects(callResult);
            }).then(function(callResult){
                //console.log("prjAddress=", callResult);
                assert.equal(callResult[0],
                    pr3_account, 
                    'ERROR: The adding of project in the list of voting work with the mistake' );
            });
        });
    });

    

     describe('The proccess of the voting started. The users try to vote. The conditions of the voting are broken'
                +'\r\n Expected behaviour: the attemps are fail. The voting has not happened ', function(){
            
            it('03.04_vote with the amount of the ether less than the minimum amount ', function() {
                    
                    hash='';
                    return MyERC20.calculateSha3(prname1).then(function(callResult){
                    //console.log("hash=", callResult);
                    balance_contract_before=web3.eth.getBalance(MyERC20.address);
                    //console.log("balance of contract before=", balance_contract_before);
                    hash=callResult;
                    return MyERC20.projects(hash);
                }).then(function(callResult){
                    //console.log("prjAddress=", callResult);
                    _voteCount_before=callResult[1];
                    _prjWeiRaised_before=callResult[2];
                    //console.log("_voteCount_before", _voteCount_before);
                    //console.log("_prjWeiRaised_before", _prjWeiRaised_before);
                    return MyERC20.vote(prname1,{from: accounts[1], value: web3.toWei(0.099,"ether")
                                          , gasPrice: web3.toWei(21,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The voting has not happened", err.toString());
                        });
                }).then(function(txResult){
                            balance_contract_after=web3.eth.getBalance(MyERC20.address);
                            //console.log("balance_contract_after=", balance_contract_after);
                    return MyERC20.projects(hash);
                }).then(function(callResult){
                    //console.log("prjAddress=", callResult);
                    _voteCount_after=callResult[1];
                    _prjWeiRaised_after=callResult[2];
                    //console.log("_voteCount_after", _voteCount_after);
                    //console.log("_prjWeiRaised_after", _prjWeiRaised_after);
                    
                    assert.equal(balance_contract_before.minus(balance_contract_after),0,
                          'ERROR: the user can vote with the amount of the ether less than the minimum amount ');
                    
              //return true;
                });
            });

            it('03.05_vote with the amount of the ether less than the minimum amount. Check voteCount', function() {
            
                assert.equal(_voteCount_before.minus(_voteCount_after), 0,
                    'ERROR: the user can vote with the amount of the ether less than the minimum amount ' );
             });

            it('03.06_vote with the amount of the ether less than the minimum amount. Check raised wei', function() {
            
                assert.equal(_prjWeiRaised_before.minus(_prjWeiRaised_after), 0,
                    'ERROR: the user can vote with the amount of the ether less than the minimum amount ' );
             });

        //******//

        it('03.07_vote for the project which dont exists in the list of the voting ', function() {
                    
                    hash='';
                    return MyERC20.calculateSha3(prname_wrong).then(function(callResult){
                    //console.log("hash=", callResult);
                    balance_contract_before=web3.eth.getBalance(MyERC20.address);
                    //console.log("balance of contract before=", balance_contract_before);
                    //console.log("balance_acc_before=", balance_acc_before);
                    hash=callResult;
                    return MyERC20.projects(hash);
                }).then(function(callResult){
                    //console.log("prjAddress=", callResult);
                    return MyERC20.vote(prname_wrong,{from: accounts[1], value: web3.toWei(0.2,"ether")
                                          , gasPrice: web3.toWei(21,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The voting has not happened", err.toString());
                        });
                }).then(function(txResult){
                            balance_contract_after=web3.eth.getBalance(MyERC20.address);
                            console.log("balance_contract_after=", balance_contract_after);
                    assert.equal(balance_contract_before.minus(balance_contract_after),0,
                          'ERROR: the user can vote for the project which dont exists in the list of the voting   ');
                    
              //return true;
                });
            });
        });
     




    describe('The users vote for the project. There are all conditions to it happens'
                +'\r\n Expected behaviour: the attempts are successful. The balance of the contract has changed.', function(){
             

        it('03.08_vote for the first project', function() {
                    _currentWinner='';
                    hash='';
                    return MyERC20.calculateSha3(prname1).then(function(callResult){
                    //console.log("hash=", callResult);
                    balance_contract_before=web3.eth.getBalance(MyERC20.address);
                    balance_acc_before=web3.eth.getBalance(accounts[1]);
                    //console.log("balance of contract before=", balance_contract_before);
                    //console.log("balance_acc_before=", balance_acc_before);
                    hash=callResult;
                    return MyERC20.currentWinner.call();
                }).then(function(callResult){
                    _currentWinner_before=callResult;
                    //console.log('_currentWinner_before=', _currentWinner_before);
                    return MyERC20.projects(hash);
                }).then(function(callResult){
                    console.log("prjAddress=", callResult);
                    _voteCount_before=callResult[1];
                    _prjWeiRaised_before=callResult[2];
                    //console.log("_voteCount_before", _voteCount_before);
                    //console.log("_prjWeiRaised_before", _prjWeiRaised_before);
                    return MyERC20.vote(prname1,{from: accounts[1], value: web3.toWei(vote_sum1,"wei")
                                          , gasPrice: web3.toWei(21,"Gwei")});
                }).then(function(txResult){
                            event=txResult.logs.find(e => e.event === 'NewBet').event;
                            balance_contract_after=web3.eth.getBalance(MyERC20.address);
                            balance_acc_after=web3.eth.getBalance(accounts[1]);
                            gasUsedwei= txResult.receipt.cumulativeGasUsed
                                * web3.eth.getTransaction(txResult.receipt.transactionHash).gasPrice.toNumber();
                            //console.log("balance_contract_after=", balance_contract_after);
                            //console.log("balance_acc_after=", balance_acc_after);
                            //console.log("gasUsedwei=", gasUsedwei);
                            return MyERC20.currentWinner.call();
                }).then(function(callResult){            
                    _currentWinner_after=callResult;
                    //console.log('_currentWinner_after=', _currentWinner_after);
                    return MyERC20.projects(hash);
                }).then(function(callResult){
                    //console.log("prjAddress=", callResult);
                    _voteCount_after=callResult[1];
                    _prjWeiRaised_after=callResult[2];
                    //console.log("_voteCount_after", _voteCount_after);
                   //console.log("_prjWeiRaised_after", _prjWeiRaised_after);
                    
                    assert.equal(balance_contract_after.minus(balance_contract_before),vote_sum1,
                          'ERROR: the voting works with the mistake-the balance of the contract is incorrect');
                    
              //return true;
                });
            });

            it('03.09_vote for the first project. Check voteCount', function() {
            
                assert.equal(_voteCount_after.minus(_voteCount_before), 1,
                    'ERROR: the voting works with the mistake - voteCount' );
             });

            it('03.10_vote for the first project. Check the raised wei', function() {
            
                assert.equal(_prjWeiRaised_after.minus(_prjWeiRaised_before), vote_sum1,
                    'ERROR: the voting works with the mistake - raised wei' );
            });

            it('03.11_vote for the first project. Check the balance of the sender', function() {
                    
                assert.equal(balance_acc_before.minus(balance_acc_after).minus(gasUsedwei).minus(vote_sum1), 0,
                    'ERROR: the voting works with the mistake- the balance of the transaction sender' );
            });

            it('03.12_vote for the first project. Check the winner', function() {
            
                assert.equal(_currentWinner_after,prname1,
                    'ERROR: the voting works with the mistake-the winner' );
            });

            it('03.13_vote for the first project. Check the event', function() {
            
                assert.equal(event,'NewBet',
                    'ERROR: the voting works with the mistake-the event' );
            });


    });                   


    describe('The winner tries to withdraw. There are not  all conditions to it happens'
                        +'\r\n Expected behaviour: the attempt is fail. The balance of the contract has not changed.', function(){

        it('03.14_withdraw the winner found when the voting has not finished', function() {
            

                        balance_contract_before=web3.eth.getBalance(MyERC20.address);
                        balance_pr1_before=web3.eth.getBalance(pr1_account);
                        //console.log("balance of contract before=", balance_contract_before);
                        //console.log("balance_acc_pr1_before=", balance_pr1_before);
                        return MyERC20.withdrawWinner({from: pr1_account
                                              , gasPrice: web3.toWei(21,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK. The withdraw has not happened", err.toString());
                }).then(function(txResult){     
                        balance_contract_after=web3.eth.getBalance(MyERC20.address);
                        balance_pr1_after=web3.eth.getBalance(pr1_account);
                        //console.log("balance of contract after=", balance_contract_before);
                        //console.log("balance_acc_pr1_after=", balance_pr1_after);
                        assert.equal(balance_contract_before.minus(balance_contract_after),0,
                               'ERROR: the withdraw has happened when the voting has not finished');
                });
        });

    });

   
});







                  



   

