var Synvotetoken = artifacts.require("./Synvote.sol");
var MyERC20;
var gasUsedwei=0;
var gasUsed=0;
var _totalSupply;
var event;
var event1;
var owner_old;
var owner_new;
var cnt;


contract('Synvotetoken functionality tests - 01',async (accounts) => {

                 
    before( function() {
        return Synvotetoken.deployed().then(function(instance){
            MyERC20 = instance;
            return true;
        });
    });

    describe('Another user (not owner) tries to change contract owner. '
                +'\r\n Expected behaviour: The attempt is failed. And the owner has not changed', function(){
         it('01.00_not owner_calls_method_of change of owner', function() {
                    owner_new=accounts[9];
                    return MyERC20.transferOwnership(owner_new, {from: accounts[1], gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. Only owner can run method of change of owner", err.toString());

               }).then(function() {
                    return MyERC20.pendingOwner.call();
                }).then(function(callResult){
                    assert.equal(callResult,'0x0000000000000000000000000000000000000000', 'ERROR:The change of owner has to start' );                
                });
        });
    });


    describe('The owner tries to change contract owner . '
                +'\r\n Expected behaviour: The process of owner changing had started. But owner had not changed. The additional steps are required', function(){
        it('01.01_owner_calls', function() {
                return MyERC20.owner.call().then(function(callResult){
                    owner_old=callResult;
                    owner_new=accounts[9];
                    return MyERC20.transferOwnership(owner_new, {from: owner_old, gasPrice: web3.toWei(1,"Gwei")});
            }).then(function() {
                    return MyERC20.pendingOwner.call();
                }).then(function(callResult){
                    assert.equal(callResult,owner_new, 'ERROR:The change of owner has not started' );
                });
        });

        it('01.01.0_check_the owner_after_first_action', function() {
                return MyERC20.owner.call().then(function(callResult){
                    assert.equal(callResult,owner_old, 'ERROR:the owner has changed! ');
                    
                });
            });
    });



    describe('Another user (not new owner) tries to end the change  of contract owner. '
                +'\r\n Expected behaviour: The attempt is failed', function(){
        it('01.01.1_not new owner_tries_to_change user after first action', function() {
                return MyERC20.claimOwnership({from: accounts[1], gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. Only owner_new can end the change of contract owner", err.toString());

               });
           });
    });

    describe(' The new owner completes the change of owner. '
                +'\r\n Expected behaviour: the sender of this transaction is new owner', function(){
        it('01.01.2_new_owner_is_finishing_the change of owner', function() {
                return MyERC20.claimOwnership({from: owner_new, gasPrice: web3.toWei(1,"Gwei")}).
                    then(function(txResult) {
                      //  console.log('txResult', txResult);
                    cnt=0;
                    for (var i = 0; i < txResult.logs.length; i++) {
                        var log = txResult.logs[i];
                        if (log.event == "OwnershipTransferred") {
                          cnt+=1;
                        }
                    }
                assert.notEqual(cnt, 0, 'ERROR:The change of owner has not happen');
                });
            });

        it('01.01.3_check_owner_after_second_action', function() {
                return MyERC20.owner.call().then(function(callResult){
                    //console.log('owner=', callResult);
                    assert.equal(callResult,owner_new, 'ERROR:the owner has not changed!');
                    
                });
            });

          it('01.01.4_check_pendingOwner', function() {
                    return MyERC20.pendingOwner.call().then(function(callResult){
                    assert.equal(callResult,'0x0000000000000000000000000000000000000000', 'ERROR:The change of owner has ended incorrectly!' );                
                });
        });
    });
});







                  



   

