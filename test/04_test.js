const { latestTime } = require('./helpers/latestTime');
const { increaseTimeTo, duration } = require('./helpers/increaseTime');
var Synvotetoken = artifacts.require("./Synvote.sol");
var MyERC20;
var gasUsedwei=0;
var gasUsed=0;
var _currentStage;
var prname1='project_one'
var prname2='project_two'
var prname3='project_three'
var prname5='project_five'
var prname_wrong='project_four'
var _owner;
var _hash1;
var _hash2;
var _hash3;
var _hash5;
var _hash_winner;
var _voteCount1_after;
var _voteCount2_after;
var _voteCount3_after;
var _voteCount5_before;
var _voteCount5_after;
var _prjWeiRaised1_after;
var _prjWeiRaised2_after;
var _prjWeiRaised3_after;
var _prjWeiRaised5_before;
var _prjWeiRaised5_after;
var votefinish;//=1546214400;  //20181231
var vote_finish;
var event='';
var balance_contract_before;
var balance_contract_after;
var balance_voter1_before;
var balance_voter1_after;
var balance_voter2_before;
var balance_voter2_after;
var balance_winner_before;
var balance_winner_after;
var winner_account;
var _currentWinner_before;
var _currentWinner_after;
var vote_sum=100000000000000000;
var balance_pr1_before;
var balance_pr1_after;
var pr1_account;
var pr2_account;
var pr3_account;
var pr5_account;
var voter1;
var voter2;
//all counts must be unequal//
var i_count1=9;
var i_count2=11;
var i_count3=10;
var winner;
var gasUsedwei_s;
var current_balance; //of the contract
var date_block;
var date1;
var b;
var j;
var addr_contract_before;
var addr_contract_after;




contract('Synvotetoken functionality tests - 04',async (accounts) => {

                 
    before( async function() {
        return Synvotetoken.deployed().then(function(instance){
            MyERC20 = instance;
            return MyERC20.currentStage.call();
         }).then(function(callResult){
                   //console.log("currentStage=", callResult);
                   _currentStage=callResult;
                       return MyERC20.owner.call();
          }).then(function(callResult){

                    //console.log("owner=", callResult);
                    owner=callResult;
                    pr1_account=accounts[5];
                    pr2_account=accounts[6];
                    pr3_account=accounts[7];
                    pr5_account=accounts[8];
                    voter1=accounts[1];
                    voter2=accounts[2];
                   return MyERC20.addProjectToVote(prname1, pr1_account,
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")});
         }).then(function(txResult){
               return MyERC20.addProjectToVote(prname2, pr2_account,
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")});
         }).then(function(txResult){
               return MyERC20.addProjectToVote(prname3, pr3_account,
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")});
         }).then(function(txResult){
               return MyERC20.addProjectToVote(prname5, pr5_account,
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")});
         }).then(function(txResult){
                //we define who must be winner for the test
               if (i_count1>i_count2&i_count1>i_count3) 
                    {winner=prname1};
                if (i_count2>i_count1&i_count2>i_count3) 
                    {winner=prname2};
                if (i_count3>i_count1&i_count3>i_count2) 
                    {winner=prname3};
                     return true;
                
        });
    });


    describe('define the date of the finish', function(){
            
        it('04.01_create the date of the finish ', async function () { 
                date_block = await latestTime();
                //console.log("date_block=", date_block);
                votefinish=date_block+duration.seconds(30);//20 seconds until the end of the voting
               // console.log("votefinish=", votefinish);
                return MyERC20.startVote(votefinish,
                                                    {from: owner, gasPrice: web3.toWei(1,"Gwei")}).then(function(txResult){
                return true;
           });
        });
    });

     describe('The proccess of voting started. The users try to vote. There are all conditions to it happens'
                +'\r\n Expected behaviour: the attemps are succesful. The votings have happened ', function(){
            
        it('04.01_the voter1 votes for the project1 ', function() {
                    _currentWinner='';
                    return MyERC20.calculateSha3(prname1).then(function(callResult){
                        //console.log("hash1=", callResult);
                        _hash1=callResult;
                        balance_contract_before=web3.eth.getBalance(MyERC20.address);
                        balance_voter1_before=web3.eth.getBalance(voter1);
                        //console.log("balance of contract before=", balance_contract_before);
                        //console.log("balance_voter1_before=", balance_voter1_before);
                    return MyERC20.currentWinner.call();
                }).then(function(callResult){
                    _currentWinner_before=callResult;
                    //console.log('_currentWinner_before=', _currentWinner_before);
                    // the voting for the first project
                    for (var i=0; i<i_count1; i++) {
                        MyERC20.vote(prname1,{from: voter1, value: web3.toWei(vote_sum,"wei")
                                              , gasPrice: web3.toWei(21,"Gwei")});
                    }
                            
                })
        });

        it('04.02_the voter1 votes for the project1. Check the count of the vote ', function() {
                    return MyERC20.projects(_hash1).then(function(callResult){

                    _voteCount1_after=callResult[1];
                    _prjWeiRaised1_after=callResult[2];
                    //console.log("_voteCount1_after", _voteCount1_after.toNumber());
                    //console.log("_prjWeiRaised1_after", _prjWeiRaised1_after.toNumber());
                    balance_contract_after=web3.eth.getBalance(MyERC20.address);
                    //console.log('balance_contract_after=', balance_contract_after.toNumber());
                    balance_voter1_after=web3.eth.getBalance(voter1);
                    //console.log('balance_voter1_after=', balance_voter1_after.toNumber());
                    assert.equal(_voteCount1_after, i_count1,"ERROR: the count of the vote has to be "+i_count1.toString());
                });
        }); 

        it('04.03_the voter1 votes for the project1. Check the raised ether for it ', function() {
                    assert.equal(_prjWeiRaised1_after, i_count1*vote_sum,
                        "ERROR: the raised ether of the project has to be "+(i_count1*vote_sum).toString());

        });
                
        
        it('04.04_the voter1 votes for the project1. Check the balance the contract ', function() {
                    current_balance=_prjWeiRaised1_after;
                    assert.equal(balance_contract_after.minus(current_balance),0,
                        "ERROR: the balance of the contract has to be "+(i_count1*vote_sum).toString());

        });         

        it('04.05_the voter1 votes for the project1. Check the current winner now', function() {
            return MyERC20.currentWinner.call().then(function(callResult){
                //console.log("currentWinner=", callResult);
                assert.equal(callResult, prname1,"ERROR: the current winner has to be "+callResult);

            });                 
        }); 


        it('04.06_the voter2 votes for the project2 ', function() {
                    _currentWinner='';
                    return MyERC20.calculateSha3(prname2).then(function(callResult){
                  //      console.log("hash2=", callResult);
                        _hash2=callResult;
                        balance_contract_before=web3.eth.getBalance(MyERC20.address);
                        balance_voter2_before=web3.eth.getBalance(voter2);
                    //    console.log("balance of contract before=", balance_contract_before);
                      //  console.log("balance_voter2_before=", balance_voter2_before);
                    return MyERC20.currentWinner.call();
                }).then(function(callResult){
                    _currentWinner_before=callResult;
                    //console.log('_currentWinner_before=', _currentWinner_before);
                    // the voting for the second project
                    for (var i=0; i<i_count2; i++) {
                        MyERC20.vote(prname2,{from: voter2, value: web3.toWei(vote_sum,"wei")
                                              , gasPrice: web3.toWei(21,"Gwei")});
                    }
                            
                })
        });

        it('04.07_the voter 2 votes for the project2. Check the count of the vote ', function() {
                    return MyERC20.projects(_hash2).then(function(callResult){

                    _voteCount2_after=callResult[1];
                    _prjWeiRaised2_after=callResult[2];
                    //console.log("_voteCount2_after", _voteCount2_after.toNumber());
                    //console.log("_prjWeiRaised2_after", _prjWeiRaised2_after.toNumber());
                    balance_contract_after=web3.eth.getBalance(MyERC20.address);
                    //console.log('balance_contract_after=', balance_contract_after.toNumber());
                    balance_voter2_after=web3.eth.getBalance(voter2);
                    //console.log('balance_voter2_after=', balance_voter2_after.toNumber());
                    //return true;
                    assert.equal(_voteCount2_after.minus(i_count2),0,"ERROR: the count of the vote has to be "+i_count2.toString());
                });
        }); 

        it('04.08_the voter 2 votes for the project2. Check the raised ether for it ', function() {
                    assert.equal(_prjWeiRaised2_after, i_count2*vote_sum,
                        "ERROR: the raised ether of the project has to be "+(i_count2*vote_sum).toString());
                      //return true;

        });
                
        
        it('04.09_the voter 2 votes for the project2. Check the balance the contract ', function() {
                    current_balance=current_balance.add(_prjWeiRaised2_after);
                    assert.equal(balance_contract_after.minus(current_balance),0,
                        "ERROR: the balance of the contract has to be "+current_balance.toString());
                     // return true;

        }); 

        it('04.10_the voter1 votes for the project3 ', function() {
                    _currentWinner='';
                    return MyERC20.calculateSha3(prname3).then(function(callResult){
                        //console.log("hash3=", callResult);
                        _hash3=callResult;
                        balance_contract_before=web3.eth.getBalance(MyERC20.address);
                        balance_voter1_before=web3.eth.getBalance(voter1);
                        //console.log("balance of contract before=", balance_contract_before);
                        //console.log("balance_voter1_before=", balance_voter1_before);
                    return MyERC20.currentWinner.call();
                }).then(function(callResult){
                    _currentWinner_before=callResult;
                    //console.log('_currentWinner_before=', _currentWinner_before);
                    // the voting for the first project
                    for (var i=0; i<i_count3; i++) {
                        MyERC20.vote(prname3,{from: voter1, value: web3.toWei(vote_sum,"wei")
                                              , gasPrice: web3.toWei(21,"Gwei")});
                    }
                            
                });
        });

        it('04.11_the voter 1 votes for the project3. Check the count of the vote ', function() {
                    return MyERC20.projects(_hash3).then(function(callResult){
                    _voteCount3_after=callResult[1];
                    _prjWeiRaised3_after=callResult[2];
                    //console.log("_voteCount3_after", _voteCount3_after.toNumber());
                    //console.log("_prjWeiRaised3_after", _prjWeiRaised3_after.toNumber());
                    balance_contract_after=web3.eth.getBalance(MyERC20.address);
                    //console.log('balance_contract_after=', balance_contract_after.toNumber());
                    balance_voter1_after=web3.eth.getBalance(voter1);
                    //console.log('balance_voter1_after=', balance_voter1_after.toNumber());
                    //return true;
                    assert.equal(_voteCount3_after.minus(i_count3),0,"ERROR: the count of the vote has to be "+i_count3.toString());
                });
                
        }); 

        it('04.12_the voter 1 votes for the project3. Check the raised ether for it ', function() {
                    assert.equal(_prjWeiRaised3_after, i_count3*vote_sum,
                        "ERROR: the raised ether of the project has to be "+(i_count3*vote_sum).toString());
                      //return true;

        });
                
        
        it('04.13_the voter 1 votes for the project3. Check the balance the contract ', function() {
                    current_balance=current_balance.add(_prjWeiRaised3_after);
                    assert.equal(balance_contract_after.minus(current_balance),0,
                        "ERROR: the balance of the contract has to be "+current_balance.toString());
                     // return true;
        }); 


        it('04.14_the voting finishes. Check the vinner ', function() {
                    return MyERC20.currentWinner.call().then(function(callResult){
                        //console.log("winner=", winner);
                        //console.log("_currentWinner=", callResult);
            
                    assert.equal(winner, callResult,
                       "ERROR: the winner has to be "+winner);
                    //  return true;
            });              
        }); 
    });


    describe('finish the voting. There all conditions for it'
                +'\r\n Expected behaviour: The attempt is succesful', function(){

        it('04.15_date_voting_finish', async function () { 
            b=0;
            j=0;
//            console.log("date_block=", date_block);
  //          console.log("votefinish=", votefinish);
    //        console.log("date_now!!!", Date.now());
                    for (var j=0; j=1000000; j++) {
                        b=b+1;
                        if (Date.now()>votefinish*1000){
                            //console.log("break=","ДАААААА!");
                            break;}
                    }
            //console.log("votefinish_AFTER=", votefinish);
            //console.log("date_now_AFTER!!!", Date.now());
                return true;
          });

        it('04.16_the voter1 votes for the project5 ', function() {
                    _currentWinner='';
                    return MyERC20.calculateSha3(prname5).then(function(callResult){
                        //console.log("hash5=", callResult);
                        _hash5=callResult;
                        balance_contract_before=web3.eth.getBalance(MyERC20.address);
                        balance_voter1_before=web3.eth.getBalance(voter1);
                        //console.log("balance of contract before=", balance_contract_before);
                        //console.log("balance_voter1_before=", balance_voter1_before);
                    return MyERC20.currentWinner.call();
                }).then(function(callResult){
                    _currentWinner_before=callResult;
                    //console.log('_currentWinner_before=', _currentWinner_before);
                    // the voting for the fifth project
                    return MyERC20.vote(prname5,{from: voter1, value: web3.toWei(vote_sum,"wei")
                                              , gasPrice: web3.toWei(21,"Gwei")});
                }).then(function(txResult){
                    event=txResult.logs.find(e => e.event === 'VoteFinished').event;
                    return true;
                });
        });
        
        it('04.17_the voter1 votes for the project5. Check the count of the vote ', function() {
                    return MyERC20.projects(_hash5).then(function(callResult){

                    _voteCount5_after=callResult[1];
                    _prjWeiRaised5_after=callResult[2];
                    //console.log("_voteCount5_after", _voteCount5_after.toNumber());
                    //console.log("_prjWeiRaised5_after", _prjWeiRaised5_after.toNumber());
                    balance_contract_after=web3.eth.getBalance(MyERC20.address);
                    //console.log('balance_contract_after=', balance_contract_after.toNumber());
                    balance_voter1after=web3.eth.getBalance(voter1);
                    //console.log('balance_voter1_after=', balance_voter1_after.toNumber());
                    assert.equal(_voteCount5_after, 1,"ERROR: the count of the vote has to be 1");
                });                
        }); 

        it('04.18_the voter1 votes for the project5. Check the raised ether for it ', function() {
                    assert.equal(_prjWeiRaised5_after, vote_sum,
                        "ERROR: the raised ether of the project has to be "+(vote_sum).toString());
        });
                
        
        it('04.19_the voter1 votes for the project5. Check the balance the contract ', function() {
                    current_balance=current_balance.add(_prjWeiRaised5_after);
                    assert.equal(balance_contract_after.minus(current_balance),0,
                        "ERROR: the balance of the contract has to be "+(current_balance).toString());
        });        

        it('04.20_the voter1 votes for the project5. Check the current stage ', function() {
                    return MyERC20.currentStage.call().then(function(callResult){
//                        console.log("currentStage=",callResult);
                    assert.equal(callResult,2,
                        "ERROR: the voting has not finished");
              });        
        });


        it('04.21_the voter1 votes for the project5. Check the event ', function() {
                    assert.equal(event,"VoteFinished",
                        "ERROR: the event has to be VoteFinished");
        });        
        
    });

    describe('The voting has finished. The voter tries to vote for the project'
                +'\r\n Expected behaviour: The attempt is fail. The balance of the contract has not changed', function(){

        it('04.22_check the voting after the finish', async function () { 
           
                        _currentWinner='';
                        return MyERC20.calculateSha3(prname5).then(function(callResult){
                            //console.log("hash5=", callResult);
                            _hash5=callResult;
                            balance_contract_before=web3.eth.getBalance(MyERC20.address);
                            balance_voter1_before=web3.eth.getBalance(voter1);
                            //console.log("balance of contract before=", balance_contract_before);
                            //console.log("balance_voter1_before=", balance_voter1_before);
                        return MyERC20.currentWinner.call();
                    }).then(function(callResult){
                        _currentWinner_before=callResult;
                        return MyERC20.projects(_hash5)
                    }).then(function(callResult){
                        _voteCount5_before=callResult[1];
                       // console.log("_voteCount5_BEFORe", _voteCount5_before.toNumber());
                        _prjWeiRaised5_before=callResult[2];
                        //console.log("_prjWeiRaised5_BEFORE", _prjWeiRaised5_before.toNumber());
                        //console.log('_currentWinner_before=', _currentWinner_before);
                        // the voting for the fifth project
                        return MyERC20.vote(prname5,{from: voter1, value: web3.toWei(vote_sum,"wei")
                                                  , gasPrice: web3.toWei(21,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK. The voting has not happened.The voting has already finished!", err.toString());
                            });
                    }).then(function(txResult){
                        return true;
                    });
            });
            
        it('04.23_check the voting after the finish. Check the count of the vote ', function() {
                    return MyERC20.projects(_hash5).then(function(callResult){

                    _voteCount5_after=callResult[1];
                    _prjWeiRaised5_after=callResult[2];
                    //console.log("_voteCount5_after", _voteCount5_after.toNumber());
                    //console.log("_prjWeiRaised5_after", _prjWeiRaised5_after.toNumber());
                    balance_voter1after=web3.eth.getBalance(voter1);
                    //console.log('balance_voter1_after=', balance_voter1_after.toNumber());
                    assert.equal(_voteCount5_after.minus(_voteCount5_before),0,
                                    "ERROR: the count of the vote has to be "+_voteCount5_before.toString());

                });
                
        }); 

        it('04.24_check the voting after the finish. Check the raised ether for it ', function() {
                    //console.log("_prjWeiRaised5_before=", _prjWeiRaised5_before);
                    //console.log("_prjWeiRaised5_after=", _prjWeiRaised5_after);
                    assert.equal(_prjWeiRaised5_after.minus(_prjWeiRaised5_before),0,
                        "ERROR: the raised ether of the project has to be "+(_prjWeiRaised5_before).toString());

        });
                
        
        it('04.25_check the voting after the finish. Check the balance the contract ', function() {
                    //console.log("balance_contract_before=", balance_contract_before);
                    //console.log("current_balance=", current_balance);
                    assert.equal(balance_contract_before.minus(current_balance),0,
                        "ERROR: the balance of the contract has to be "+(current_balance).toString());
                    

        });        

    });    

    describe('The other user (not winner) tries to withdraw the award.'
                        +'\r\n Expected behaviour: the attempt is fail. The balance of the contract has not changed.', function(){

        it('04.26_other user (not winner) tries to withdraw the award', function() {
            

                        balance_contract_before=web3.eth.getBalance(MyERC20.address);
                      //  console.log("balance of contract before=", balance_contract_before);
                        return MyERC20.withdrawWinner({from: accounts[9]
                                              , gasPrice: web3.toWei(21,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK. The user (not winner) cant get the award", err.toString());
                }).then(function(txResult){     
                        balance_contract_after=web3.eth.getBalance(MyERC20.address);
                        //console.log("balance of contract after=", balance_contract_before);
                        assert.equal(balance_contract_before.minus(balance_contract_after),0,
                               'ERROR: the other user (not winner) has gotten the award');
                });
        });

    });

    describe('The  owner tries to call the method kill. There are not all condition to it happens'
                        +'\r\n Expected behaviour: the attempt is fail. ', function(){
        it('04.27_call the method kill', function() {
            return MyERC20.kill({from: owner
                                              , gasPrice: web3.toWei(21,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK. The kill of the contract has not happened!", err.toString());
                            });
                        
        });
    });


    describe('The winner tries to withdraw the award.'
                        +'\r\n Expected behaviour: the attempt is succesful. The balance of the contract has changed.', function(){

        it('04.28_the winner tries to withdraw the award', function() {
            

                        balance_contract_before=web3.eth.getBalance(MyERC20.address);
                        //console.log("balance of contract before=", balance_contract_before);
                        return MyERC20.calculateSha3(winner).then(function(callResult){
                          //  console.log("hash_winner=", callResult);
                            _hash_winner=callResult;
                        return MyERC20.projects(_hash_winner)
                    }).then(function(callResult){
                            winner_account=callResult[0];
                            //console.log("winner_account=", winner_account);
                            balance_winner_before=web3.eth.getBalance(winner_account);
                            //console.log("balance_winner_before=", balance_winner_before);
                        return MyERC20.withdrawWinner({from: winner_account
                                              , gasPrice: web3.toWei(21,"Gwei")});
                    }).then(function(txResult){     
                            gasUsedwei= txResult.receipt.cumulativeGasUsed
                                * web3.eth.getTransaction(txResult.receipt.transactionHash).gasPrice.toNumber();
                            return MyERC20.currentStage.call();
                    }).then(function(callResult){     
                            _currentStage=callResult;
                            balance_contract_after=web3.eth.getBalance(MyERC20.address);
                            //console.log("balance of contract after=", balance_contract_after);
                            assert.equal(callResult,3, "ERROR: the withdraw has happened incorrect")
                            //assert.equal(balance_contract_after,0,
                            //       'ERROR: the winner cant get the award');
                    });
        });


        it('04.29_the winner tries to withdraw the award.Check the balance of the contract', function() {
                        assert.equal(balance_contract_after,0,
                                  'ERROR: the winner cant get the award');
        });

        it('04.30_the winner tries to withdraw the award.Check the balance of the winner', function() {
                        balance_winner_after=web3.eth.getBalance(winner_account);
                        //console.log("balance_winner_after=", balance_winner_after);
                        //console.log("gasUsedwei=", gasUsedwei);
                        //console.log("balance_winner_before=", balance_winner_before);
                        //console.log("balance_of_the_contract=", balance_contract_before);
                        assert.equal(balance_winner_after.add(gasUsedwei).minus(balance_winner_before).minus(balance_contract_before)
                                    ,0,
                                  'ERROR: the balance of the winner is incorrect');
        });

         it('04.31_the winner tries to withdraw the award.Check the stage', function() {
                        assert.equal(_currentStage,3,
                                  'ERROR: the stage has to be rewardWithdrawn');
        });
    });

    describe('The user (not owner) tries to call the method kill'
                        +'\r\n Expected behaviour: the attempt is fail. ', function(){
        it('04.32_call the method kill', function() {
            return MyERC20.kill({from: accounts[1]
                                              , gasPrice: web3.toWei(21,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK. The kill of the contract has not happened!", err.toString());
                            });
                        
        });
    });

    describe('The owner tries to call the method kill'
                        +'\r\n Expected behaviour: the attempt is succesful ', function(){
        it('04.33_call the method kill', function() {
            addr_contract_before=MyERC20.address;            
            balance_contract_before=web3.eth.getBalance(MyERC20.address);
            console.log("balance_contract_before=", balance_contract_before);
            console.log("addr_contract_before=", addr_contract_before);
            return MyERC20.kill({from: owner
                                              , gasPrice: web3.toWei(21,"Gwei")}).then(function(txResult){
                //console.log("The owner kill the contract!!!")
                    addr_contract_after=MyERC20.address;
                    console.log("addr_contract_after=", addr_contract_after);
                    balance_contract_after=web3.eth.getBalance(MyERC20.address);
                    console.log("balance_contract_after=", balance_contract_after);
            });
                        
        });
    });


});







                  



   

