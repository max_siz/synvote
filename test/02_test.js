const { latestTime } = require('./helpers/latestTime');
const { increaseTimeTo, duration } = require('./helpers/increaseTime');

var Synvotetoken = artifacts.require("./Synvote.sol");
var MyERC20;
var gasUsedwei=0;
var gasUsed=0;
var _currentStage;
var prname1='project_one'
var prname2='project_two'
var prname3='project_three'
var _owner;
var _hash;
var votefinish;
var votefinish_old=1544400000  //20181210
var vote_finish;
var event;
var _voteCount_before;
var _prjWeiRaised_before;
var _voteCount_after;
var _prjWeiRaised_after;
var date_block;


contract('Synvotetoken functionality tests - 02',async (accounts) => {

                 
    before( function() {
        return Synvotetoken.deployed().then(function(instance){
            MyERC20 = instance;
            return MyERC20.currentStage.call();
         }).then(function(callResult){
                   console.log("currentStage=", callResult);
                   _currentStage=callResult;
                       return MyERC20.owner.call();
          }).then(function(callResult){
                   console.log("owner=", callResult);
                   owner=callResult;
                
            return true;
        });
    });

    describe('define the date of the finish', function(){
            
        it('02.000_create the date of the finish ', async function () { 
                date_block = await latestTime();
                //console.log("date_block=", date_block);
                votefinish=date_block+duration.seconds(20);//20 seconds until the end of the voting
                //console.log("votefinish=", votefinish);
                return true;
          
        });
    });

    describe('We add the project to vote on initial stage '
                +'\r\n Expected behaviour: the attempt is successful. The project is added to the list of the voting ', function(){
         it('02.00_check initial stage - what is it ', function() {
                  //  console.log("_currentStage=", _currentStage);
            assert.equal(_currentStage,0, 'ERROR:The initial stage is not preList' );
        });


        it('02.01_Other user (not owner) tries to add the project to the list of the voting', function() {
             return MyERC20.addProjectToVote(prname1, '0x32f4d43c74663ae00fe7f58ec9649f0a30bd3940',
                                                {from: accounts[8], gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The adding of the project has not happened in the list of the voting .", err.toString());
              
            }).then(function(txResult){
                return MyERC20.calculateSha3(prname1);
            }).then(function(callResult){
                //console.log("hash=", callResult);
                return MyERC20.projects(callResult);
            }).then(function(callResult){
                //console.log("prjAddress=", callResult);
                assert.equal(callResult[0],
                    '0x0000000000000000000000000000000000000000', 
                    'ERROR: The other user (not owner) can add the project to the list of the voting ' );
                
   
            });
        });


        it('02.02_The owner tries to add the project in first time to the list of the voting', function() {
             return MyERC20.addProjectToVote(prname1, '0x32f4d43c74663ae00fe7f58ec9649f0a30bd3940',
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")}).then(function(txResult){
                return MyERC20.calculateSha3(prname1);
            }).then(function(callResult){
                //console.log("hash=", callResult);
                return MyERC20.projects(callResult);
            }).then(function(callResult){
               // console.log("prjAddress=", callResult);
                _voteCount_before=callResult[1];
                _prjWeiRaised_before=callResult[2];
                assert.equal(callResult[0],
                    '0x32f4d43c74663ae00fe7f58ec9649f0a30bd3940', 
                    'ERROR: The adding of the project has happened with the mistake' );
            });
        });

        it('02.03_The owner tries to add the project in first time to the list of the voting.Check the voteCount', function() {
            
                assert.equal(_voteCount_before,0, 'ERROR: The adding of the project has happened with the mistake' );
        });

        it('02.04_The owner tries to add the project in first time to the list of the voting.Check the prjWeiRaised', function() {
            
                assert.equal(_prjWeiRaised_before,0, 'ERROR: The adding of the project has happened with the mistake' );
        });



        it('02.05_The owner tries to add the project with the address 0 to the list of the voting', function() {
             return MyERC20.addProjectToVote(prname2, '0x0000000000000000000000000000000000000000',
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The adding of the project has not happened in the list of the voting .", err.toString());


            }).then(function(txResult){
                return MyERC20.calculateSha3(prname2);
            }).then(function(callResult){
               // console.log("hash=", callResult);
                return MyERC20.projects(callResult);
            }).then(function(callResult){
               // console.log("prjAddress=", callResult);
                assert.equal(callResult[0],
                    '0x0000000000000000000000000000000000000000', 
                    'ERROR: The owner can add the project with address 0 to the list of the voting ' );
                
   
            });
        });


        it('02.06_The owner tries to add the project in second time with new addrress to the list of the voting', function() {
             return MyERC20.addProjectToVote(prname1, '0xa4b97c03952f132555000269c2067533d73cacff',
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The adding of the project in second time has not happened in the list of the voting .", err.toString());


            }).then(function(txResult){
                return MyERC20.calculateSha3(prname1);
            }).then(function(callResult){
                //console.log("hash=", callResult);
                return MyERC20.projects(callResult);
            }).then(function(callResult){
               // console.log("prjAddress=", callResult);
                assert.equal(callResult[0],
                    '0x32f4d43c74663ae00fe7f58ec9649f0a30bd3940', 
                    'ERROR: The adding of project is possible in second time' );
                
   
            });
        });

        it('02.07_The owner tries to add the project in second time with old address to the list of the voting', function() {
             return MyERC20.addProjectToVote(prname1, '0x32f4d43c74663ae00fe7f58ec9649f0a30bd3940',
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The adding of the project in second time with old address has not happened in the list of the voting .", err.toString());


            }).then(function(txResult){
                return MyERC20.calculateSha3(prname1);
            }).then(function(callResult){
                //console.log("hash=", callResult);
                return MyERC20.projects(callResult);
            }).then(function(callResult){
                //console.log("prjAddress=", callResult);
                assert.equal(callResult[0],
                    '0x32f4d43c74663ae00fe7f58ec9649f0a30bd3940', 
                    'ERROR: The adding of project is possible in second time' );
                
   
            });
        });

    });



    describe('The proccess of voting has not started. The users try to vote. The conditions of the voting are broken'
                +'\r\n Expected behaviour: the attemps are fail. The voting has not happened ', function(){
            
            it('02.08_vote when the vote has not started ', function() {
                    
                    hash='';
                    return MyERC20.calculateSha3(prname1).then(function(callResult){
                    //console.log("hash=", callResult);
                    balance_contract_before=web3.eth.getBalance(MyERC20.address);
                    //console.log("balance of contract before=", balance_contract_before);
                    hash=callResult;
                    return MyERC20.projects(hash);
                }).then(function(callResult){
                    //console.log("prjAddress=", callResult);
                    _voteCount_before=callResult[1];
                    _prjWeiRaised_before=callResult[2];
                    //console.log("_voteCount_before", _voteCount_before);
                    //console.log("_prjWeiRaised_before", _prjWeiRaised_before);
                    return MyERC20.vote(prname1,{from: accounts[1], value: web3.toWei(0.2,"ether")
                                          , gasPrice: web3.toWei(21,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The voting has not happened. The voting has not started yet", err.toString());
                        });
                }).then(function(txResult){
                            balance_contract_after=web3.eth.getBalance(MyERC20.address);
                      //      console.log("balance_contract_after=", balance_contract_after);
                    return MyERC20.projects(hash);
                }).then(function(callResult){
                    //console.log("prjAddress=", callResult);
                    _voteCount_after=callResult[1];
                    _prjWeiRaised_after=callResult[2];
                  //  console.log("_voteCount_after", _voteCount_after);
                  //  console.log("_prjWeiRaised_after", _prjWeiRaised_after);
                    
                    assert.equal(balance_contract_before.minus(balance_contract_after),0,
                          'ERROR: the user can vote when the voting has not started yet ');
                    
              
                });
            });
        });


    describe('The different users try to start the the voting'
                +'\r\n Expected behaviour: the attempt is successful only  for owner. The voting started ', function(){

        it('02.09_The other user (not owner) tries to start the voting.', function() {
             return MyERC20.startVote(votefinish,
                                                {from: accounts[8], gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The owner can only start the voting .", err.toString());


            }).then(function(txResult){
                return MyERC20.currentStage.call();
            }).then(function(callResult){
                //console.log("currentStage=", callResult);
                assert.equal(callResult,0, 'ERROR: the other user (not owner) can start the voting')
            });
        });

        it('02.10_The owner tries to start the voting from old date.', function() {
             return MyERC20.startVote(votefinish_old,
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The owner cant start the voting with the  old date .", err.toString());


            }).then(function(txResult){
                return MyERC20.currentStage.call();
            }).then(function(callResult){
                //console.log("currentStage=", callResult);
                assert.equal(callResult,0, 'ERROR: the owner can start the voting with the old date')
            });
        });

         it('02.11_The owner tries to start the voting. There are all conditions for it', function() {
             return MyERC20.startVote(votefinish,
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")}).then(function(txResult){
                event=txResult.logs.find(e => e.event === 'VoteStarted').event;
                return MyERC20.voteFinishDate.call();
            }).then(function(callResult){
                vote_finish=callResult;
                return MyERC20.currentStage.call();
            }).then(function(callResult){
                //console.log("currentStage=", callResult);
                assert.equal(callResult,1, 'ERROR: the owner cant start the voting')
            });
        });

        it('02.12_The owner tries to start the voting. There are all conditions for it. Check event', function() {
            
                assert.equal(event,'VoteStarted', 'ERROR: The start of the voting has happened with the mistake' );
        });

        it('02.13_The owner tries to start the voting. There are all conditions for it. Check date', function() {
            
                assert.equal(vote_finish,votefinish, 'ERROR: The start of the voting has happened with the mistake' );
        });

         it('02.14_The owner tries to start the voting in second time.', function() {
             return MyERC20.startVote(votefinish+1,
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The owner cant  start the voting in second time.", err.toString());


            }).then(function(txResult){
                return MyERC20.voteFinishDate.call();
            }).then(function(callResult){
                //console.log("votefinish=", callResult);
                assert.equal(callResult,votefinish, 'ERROR: the owner can  start the voting in second time')
            });
        });

    });

     describe('The owner tries to add the project in the list of the voting. The voiting has already started'
                +'\r\n Expected behaviour: the attempt is fail. The project is not added in the list', function(){

         it('02.15_The owner tries to add the project after the start of the voting to the list of the voting', function() {
             return MyERC20.addProjectToVote(prname3, ' 0xb64ef51c888972c908cfacf59b47c1afbc0ab8ac',
                                                {from: owner, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The adding of the project  has not happened after the start of the voting to the list of the voting .", err.toString());


            }).then(function(txResult){
                return MyERC20.calculateSha3(prname3);
            }).then(function(callResult){
                //console.log("hash=", callResult);
                return MyERC20.projects(callResult);
            }).then(function(callResult){
               // console.log("prjAddress=", callResult);
                assert.equal(callResult[0],
                    '0x0000000000000000000000000000000000000000', 
                    'ERROR: The adding of project is not possible after the start of the voting' );
                
   
            });
        });
    });
   
});







                  



   

